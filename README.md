# Data mining Cienca y Tecnología - Maestría UBA 2021

## Links 

**Aula virtual**:
http://datamining.dc.uba.ar/campus/course/view.php?id=74


**zoom** 
https://exactas-uba.zoom.us/j/81653443181
ID de reunión: 816 5344 3181
Código de acceso: dmct21-me

Martes de 19 a 22 hs -> Teórica (Todos)
Viernes de 19 a 22 hs -> Práctica, opcional


## Profesores

* luzbavassi@gmail.com
* juank@dc.uba.ar

## Integrantes

* Gaspar Acevedor: 
	* acevedo.zain.gaspar@gmail.com
	* gasper-az
* Alejandra Fusco
	* alejandrafusco@gmail.com
	* alejandrafusco
* Lucas Bertolini
	* lucasb.256@gmail.com / lucasb_256@hotmail.com
	* blukitas
* Marcos Mobbili 
	* mmobbili@gmail.com
	* mmobbili
	

## Link utiles

* Drive grupos: https://docs.google.com/spreadsheets/d/1BK15eLuDkVG_ZcfHI6LXzah5I_z9EALf-gUTcCQRb0I/edit#gid=0
* Link tp campus: http://datamining.dc.uba.ar/campus/mod/assign/view.php?id=2276
* Overleaf tp: https://www.overleaf.com/9898777496jhzjchfvpnkt


## Referencias en videos

* Clase 20210824 -> 
	* Desde 1:20 - 1:40 va explicando inicio del tp, los datos, que miramos, que queremos explicar.
		* El filtro pasabanda es para filtrar ruido, por ejemplo movimiento ocular, respiración. Queda un poco más filtrada, normal, sin picos que no aportan (En teoría Luz ya lo filtro/acomodó, como para que enfoquemos en clustering).
		* Los datos van en 128 hz.
		* Uno va a buscar el cambio de estado entre reposo pre estudio y reposo post estudio
		* Objetivo: 
			* Hay ciertos estados que ayudan a explicar como cambia el sistema (El cerebro es un sistema dinámico).
			* Cada instante de nuestro archivo procesado, representa 60-150 ms de muestra. Ciertos estados 'estables'. 
			* La hipótesis es que estos estados, fragmentos de tiempos, son estables, y con eso puedo explicar el funcionamiento/la dinámica.
			* Está ventana es variable, y es parte de lo que tenemos que encontrar
	* Desde 1:40 - 2:00. Empieza con como desarrollamos el tp.
		* En cada instante uno tiene una 'coloración' (No estoy muy seguro que ese plot)
		* En cada instante saco un snapshot (30 canales, en 7861 instantes)
		* Sobre esa seguidilla de snapshots puedo armar ciertos clusters, y con eso armar los estados
		* **La hipótesis** es que estos estados existen.
		* GFP = varianza entre canales en un instante de tiempo.
		* _Donde se maximiza la varianza entre los canales_ (GFP), alrededor de esos puntos la señal se mantiene más estable - es más probable que los estados no cambien -. (Esa maximización tiene que ver con algo biológico)
		* Voy a calcular el estado en cada uno de esos puntos/peaks (No hay ancho, es en los puntos. Discretizo mi señal a esos puntos.)
		* Parte del trabajo es como identificamos los picos, si son muy cerquitas, si el siguiente es menor que el que estoy viendo. 
	* Desde 2:00 - 
		* Vamos a hacer grupos
		* En que momento se ven los picos
	* En 2:10-2:20 habla de un paper en el que hacen analisis de EGG microstates estando despierto y dormido. Clasifican con 4 microestados, y despues hacen distintas técnicas estudiando las transiciones entre microestados.
	* **Antecedente 2** -> Si hay un periodo de no tarea, pos aprendizaje, se guarda mejor la información. 
		* Resting state, estado negativo. Se le pide a la persona que no haga 'nada'.

Todo está apoyado en papers, en evidencia empirica. Es como lo hacen muchos grupos de estudios.

		