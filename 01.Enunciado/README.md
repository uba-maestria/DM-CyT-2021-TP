# Enunciado del tp

Preguntas ya respondidas:

1) ¿En el campo "srate" que figura 256 habría que cambiarlo a 128 ya que los datos que nos pasaron están en esa frecuencia? 
	* Exactamente. En la clase que di yo, los datos estaban a 256Hz porque eran los datos crudos sin filtrar y no cortados. Los que tienen que usar en el TP, ya están filtrados y la frecuencia de sampleo está a 128Hz. Por lo tanto, donde dice 256 en el codigo deben poner 128Hz.
	
2) Toda la parte de pre-procesamiento no es necesaria porque esa parte es la que ya fue realizada sobre los datos antes de compartirlos, ¿Correcto?.
	* Efectivamente. El filtrado (lo que dice pre procesamiento) ya lo hicimos antes.


## Nuestras preguntas


### Punto 02 - Gráfico de topografías de participantes
Lo que hay hacer en este punto es calcular el promedio de los valores de cada participante, para cada bloque. Esto nos va a dar como resultado un gráfico en el que no se van a ver muchas diferencias en los electrodos. Lo que buscan mostrar acá los profesores es que el promedio, para estos casos en donde suelen haber valores positivos y negativos que se anulan, no es de mucha ayuda.


### Punto 03 - Cálculo de PCA
Acá tenemos que concatenar los bloques de cada participante. Luego, a esa concatenación hay que aplicarle PCA, y graficar los loadings de las tres componentes que captamos, es decir, vamos a tener como resultado un gráfico de una cabeza en donde se remarca cuanto variabilidad capta/representa c/u de los electrodos


### Punto 04 - Referencia de los datos al promedio por sample
Básicamente tenemos que, a los bloques concatenados de cada participante, calcular el promedio en cada instante de tiempo (es decir, promedio vertical), y luego hacer la resta de los valores de c/u de los 30 electrodos y el promedio obtenido. Esto representa la diferencia que se menciona en el paper en la función de GFP de la página 03 (en el enunciado del tp no hacen referencia a la resta entre el valor del electrodo u y el promedio de los electrodos)


### Formato de entrega
Word/PDF. No es necesario entregar código, ni tampoco es necesario hacer entrega en latex :músculo:


## Otras preguntas

* Las preguntas de buttler width que rol tienen? Ayudan a suavizar y marcar la información más relvante de ese fragmento? 
	* Eso en teoría ya está resuelto con el procesado y no tendríamos que darle atención

* Entrega? 
	* Informe? 
	* Notebook + markdown?
	* Cantidad de páginas? Graficos?
	